local conditions = require("utils.luasnip.conditions")
local ls = require("utils.luasnip.snips")
local nodes = require("utils.luasnip.nodes")

local snippets = ls.snippets("go")

ls.insert(
	snippets,
	"interface",
	[[
		type {} interface {{
			{}
		}}

		{}
	]],
	{
		nodes.ins(1, "name"),
		nodes.ins(2, "funcs"),
		nodes.ins(3),
	}
)
ls.insert(
	snippets,
	"struct",
	[[
		type {} struct {{
			{}
		}}

		{}
	]],
	{
		nodes.ins(1, "name"),
		nodes.ins(2, "attrs"),
		nodes.ins(3),
	}
)

ls.insert(
	snippets,
	"fore",
	[[
		for {} := range {} {{
			{}
		}} 
		{}
	]],
	{
		nodes.ins(1, "ele"),
		nodes.ins(2, "slice"),
		nodes.ins(3, "// TODO:"),
		nodes.ins(4),
	}
)

ls.insert(
	snippets,
	"ternary",
	[[
		if {} {{
			{} = {}
		}} else {{
			{} = {}
		}}
	]],
	{
		nodes.ins(1, "condition"),
		nodes.ins(2, "variable"),
		nodes.ins(3, "assign_if_true"),
		nodes.rep(2),
		nodes.ins(4, "assign_if_false"),
	}
)

ls.insert(
	snippets,
	"fori",
	[[
		for {} := 0; {} < {}; {}++ {{
			{}
		}} 
		{}
	]],
	{
		nodes.ins(1, "i"),
		nodes.rep(1),
		nodes.choice(2, {
			nodes.ins(1, "num"),
			nodes.sn(1, {
				nodes.txt("len("),
				nodes.ins(1, "arr"),
				nodes.txt(")"),
			}),
		}),
		nodes.rep(1),
		nodes.ins(3, "// TODO:"),
		nodes.ins(4),
	}
)

ls.insert(
	snippets,
	"ternary",
	[[
		if {} {{
			{} = {}
		}} else {{
			{} = {}
		}}
	]],
	{
		nodes.ins(1, "condition"),
		nodes.ins(2, "variable"),
		nodes.ins(3, "assign_if_true"),
		nodes.rep(2),
		nodes.ins(4, "assign_if_false"),
	}
)

ls.load(snippets)
