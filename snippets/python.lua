local ls = require("luasnip")
local s = ls.snippet --> snippet
local i = ls.insert_node --> insert
local t = ls.text_node --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {}

-- local group = vim.api.nvim_create_augroup("python snippets", { clear = true })
-- local file_pattern = "*.py"

local main = s("maini", fmt('if __name__ == "__main__":\n\t{}', { i(1, { "pass" }) }))
table.insert(snippets, main)

local _function = s(
	"function",
	fmt(
		[[
		def {}({}){}:
			{}
		]],
		{
			i(1, "name"),
			i(2, "params"),
			c(3, { t(""), { t(" -> "), i(1, "type") } }),
			i(4, "pass"),
		}
	)
)
table.insert(snippets, _function)
local _method = s(
	"method",
	fmt(
		[[
		def {}(self, {}){}:
			{}
		]],
		{
			i(1, "name"),
			i(2, "params"),
			c(3, { t(""), { t(" -> "), i(1, "type") } }),
			i(4, "pass"),
		}
	)
)
table.insert(snippets, _method)
local logv = s(
	"logv",
	fmt("logger.{}", {
		c(1, {
			fmt('debug(f"{}: {{{}}}")', { i(1, "var"), rep(1) }),
			fmt('info(f"{}: {{{}}}")', { i(1, "var"), rep(1) }),
			fmt('warning(f"{}: {{{}}}")', { i(1, "var"), rep(1) }),
			fmt('critical(f"{}: {{{}}}")', { i(1, "var"), rep(1) }),
		}),
	})
)
table.insert(snippets, logv)
local logt = s(
	"logt",
	fmt("logger.{}", {
		c(1, {
			fmt('debug(f"{}")', { i(1, "var") }),
			fmt('info(f"{}")', { i(1, "var") }),
			fmt('warning(f"{}")', { i(1, "var") }),
			fmt('critical(f"{}")', { i(1, "var") }),
		}),
	})
)
table.insert(snippets, logt)

ls.add_snippets("python", snippets)
