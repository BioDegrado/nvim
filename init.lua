vim.g.mapleader = " "
local core = require("core")
core.print_on_error(pcall, { core.load_os_configs }, "Cannot load os config: ")
core.print_on_error(core.safe_require, "config.mappings")
core.print_on_error(core.safe_require, "config.options")
core.print_on_error(core.safe_require, "config.autocmd")
core.print_on_error(core.safe_require, "config.lazy")
