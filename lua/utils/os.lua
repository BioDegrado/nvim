local M = {}

--- Returns a string corresponding to the family of OS that
--- neovim is running on.
---@return string os the os can assume 3 values LINUX, WINDOWS, MAC.
---
M.get_os = function()
	local sysname = vim.uv.os_uname().sysname
	if sysname == "Linux" then
		return "LINUX"
	elseif sysname == "Windows_NT" then
		return "WINDOWS"
	elseif sysname == "Darwin" then
		return "MAC"
	end
	return "Unknown"
end

M.get_shell = function()
	local path = vim.api.nvim_get_option_value("shell", {})
	path = vim.fs.basename(path)
	if path == nil then
		return ""
	end
	path, _ = path:gsub([[%.%w*"$]], "")
	return path
end

M.is_linux = function() return vim.fn.has("unix") end
M.is_windows = function() return vim.fn.has("win32") ~= 0 end

---append or prepend a path to the path env variable
---@param path string the path to prepend/append
---@param prepend boolean if the path needs to be prepend. by default is false
M.path = function(path, prepend)
	prepend = prepend and prepend or false
	if prepend then
		vim.env.PATH = path .. ":" .. vim.env.PATH
	else
		vim.env.PATH = vim.env.PATH .. ":" .. path
	end
end

return M
