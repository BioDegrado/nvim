local M = {}
-- Common kill function for bdelete and bwipeout
-- credits: LunarVim and bbye and nvim-bufdel
---@param kill_command? string defaults to "bd"
---@param bufnr? number defaults to the current buffer
---@param force? boolean defaults to false
M.smart_close = function(kill_command, bufnr, force)
	kill_command = kill_command or "bd"

	local bo = vim.bo
	local api = vim.api
	local fmt = string.format
	local fn = vim.fn

	if bufnr == 0 or bufnr == nil then
		bufnr = api.nvim_get_current_buf()
	end

	local bufname = api.nvim_buf_get_name(bufnr)

	if not force then
		local choice
		if bo[bufnr].modified then
			choice = fn.confirm(
				fmt([[Save changes to "%s"?]], bufname),
				"&Yes\n&No\n&Cancel"
			)
			if choice == 1 then
				vim.api.nvim_buf_call(bufnr, function() vim.cmd("w") end)
			elseif choice == 2 then
				force = true
			else
				return
			end
		elseif
			api.nvim_get_option_value("buftype", { buf = bufnr }) == "terminal"
		then
			choice =
				fn.confirm(fmt([[Close "%s"?]], bufname), "&Yes\n&No\n&Cancel")
			if choice == 1 then
				force = true
			else
				return
			end
		end
	end

	-- Get list of windows IDs with the buffer to close
	local windows = vim.tbl_filter(
		function(win) return api.nvim_win_get_buf(win) == bufnr end,
		api.nvim_list_wins()
	)

	if force then
		kill_command = kill_command .. "!"
	end

	-- Get list of active buffers
	local buffers = vim.tbl_filter(
		function(buf) return api.nvim_buf_is_valid(buf) and bo[buf].buflisted end,
		api.nvim_list_bufs()
	)

	-- If there is only one buffer (which has to be the current one), vim will
	-- create a new buffer on :bd.
	-- For more than one buffer, pick the previous buffer (wrapping around if necessary)
	if #buffers > 1 and #windows > 0 then
		for i, v in ipairs(buffers) do
			if v == bufnr then
				local prev_buf_idx = i == 1 and #buffers or (i - 1)
				local prev_buffer = buffers[prev_buf_idx]
				for _, win in ipairs(windows) do
					api.nvim_win_set_buf(win, prev_buffer)
				end
			end
		end
	end

	-- Check if buffer still exists, to ensure the target buffer wasn't killed
	-- due to options like bufhidden=wipe.
	if api.nvim_buf_is_valid(bufnr) and bo[bufnr].buflisted then
		vim.cmd(string.format("%s %d", kill_command, bufnr))
	end
end

M.get_type = function(buf)
	return vim.api.nvim_get_option_value("buftype", { buf = buf })
end

--- Returns true if the current buffer type is contained in the list of buftypes
--- given in input
---@param types table a list of buf types
---@return boolean
M.current_buf_is = function(types)
	local buf = vim.api.nvim_get_current_buf()
	local btype = M.get_type(buf)
	for _, type in ipairs(types) do
		if type == btype then
			return true
		end
	end
	return false
end

--- Given the bufnr it returns its size in MB.
--- If the call is not successful it returns nil
--- given in input
---@param bufnr integer the buffer nuber
---@return boolean | nil
M.get_size = function(bufnr)
	local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(bufnr))
	-- pcall failed
	if not (ok and stats) then
		return nil
	end
	return stats.size
end

return M
