local M = {}

M.file_exists = function(path)
	local file = io.open(path, "r")
	if file ~= nil then
		io.close(file)
		return true
	end
	return false
end

--- Find files that ends with the given strings
--- @param queries string[] The list of strings of strings to match the end of the files
--- @param path string The path to search for files
--- Example:
--- ```lua
--- 		local files = M.find_files_ends_with({".lua", ".json"}, "/home/user")
--- 		vim.print(files)
--- 		-- Output: { { index = 1, file = "/home/user/file.lua" }, { index = 2, file = "/home/user/file.json" } }
--- ````
--- @return {index: integer, file: string }[]
M.find_files_ends_with = function(queries, path)
	---@type {index: integer, file: string }[]
	local res = {}
	for i, q in ipairs(queries) do
		local endswith = function(f, _)
			return f:match(string.format("(.*)%s$", q))
		end
		local found =
			vim.fs.find(endswith, { type = "file", path = path, limit = 100 })

		if found and #found > 0 then
			local maxsize = #found[1]
			local maxmatch = found[1]
			for _, filename in pairs(found) do
				if #filename < maxsize then
					maxsize = #filename
					maxmatch = filename
				end
			end
			res[#res + 1] = { index = i, file = maxmatch }
		end
	end
	return res
end

return M
