local M = {}

local ls = require("luasnip")
local fmt = require("luasnip.extras.fmt").fmt

M.snippets = function(filetype)
	return {
		filetype = filetype,
		list = {},
	}
end

M.insert = function(snippets, trigger, format, nodes, condition)
	table.insert(
		snippets.list,
		ls.snippet(trigger, fmt(format, nodes), condition)
	)
end

M.load = function(snippets) ls.add_snippets(snippets.filetype, snippets.list) end

return M
