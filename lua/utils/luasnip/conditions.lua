local M = {}

-- Credits to go.nvim:
-- https://github.com/ray-x/go.nvim/blob/master/lua/go/ts/go.lua
function M.in_func()
	local ok, ts_utils = pcall(require, "nvim-treesitter.ts_utils")
	if not ok then
		return false
	end
	local current_node = ts_utils.get_node_at_cursor()
	if not current_node then
		return false
	end
	local expr = current_node

	while expr do
		if
			expr:type() == "function_declaration"
			or expr:type() == "method_declaration"
		then
			return true
		end
		expr = expr:parent()
	end
	return false
end

return M
