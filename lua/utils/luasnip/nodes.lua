local ls = require("luasnip")
return {
	ins = ls.insert_node,
	txt = ls.text_node,
	dyn = ls.dynamic_node,
	choice = ls.choice_node,
	func = ls.function_node,
	rep = require("luasnip.extras").rep, --> repetition node
	sn = ls.snippet_node,
}
