local utilsos = require("utils.os")
return {
	cmd = {
		(utilsos.is_windows() and utilsos.get_shell() == "bash.exe")
				and "ltex-ls.cmd"
			or "ltex-ls",
	},
	filetypes = {
		"bib",
		"gitcommit",
		"markdown",
		"org",
		"plaintex",
		"rst",
		"rnoweb",
		"tex",
		"pandoc",
		"quarto",
		"rmd",
		"context",
		"mail",
		"text",
	},
	settings = {
		additionalRules = {
			enablePickyRules = false,
			motherTongue = "en",
			languageModel = "~/Ngrams/",
		},
		dictionary = {
			["en-US"] = {
				"Xu",
				"Ruixuan",
				"Casalicchio",
				"programmability",
				"TODO",
				"Markovian",
				"offloadability",
				"GBps",
				"MBps",
				"ReLU",
				"Conv",
				"underfitting",
			},
		},
	},
}
