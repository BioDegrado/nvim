local RUNTIMES = {
	Windows_NT = {
		{
			name = "JavaSE-11",
			path = vim.fn.glob("C:/Program Files/Microsoft/jdk-11.*/"),
		},
		{
			name = "JavaSE-17",
			path = vim.fn.glob("C:/Program Files/Microsoft/jdk-17.*/"),
		},
		-- {
		-- 	name = "JavaSE-21",
		-- 	path = vim.fn.glob("C:/Program Files/Microsoft/jdk-21.*/"),
		-- },
	},
	Linux = {
		{
			name = "JavaSE-11",
			path = "/usr/lib/jvm/java-11-openjdk-amd64/",
		},
		{
			name = "JavaSE-21",
			path = "/usr/lib/jvm/java-21-openjdk-amd64/",
		},
	},
}

return {
	-- cleanup = {
	-- 	"addOverride",
	-- 	saveActions = {
	-- 		organizeImports = true,
	-- 	},
	-- },
	signatureHelp = { enabled = true },
	contentProvider = { preferred = "fernflower" },
	eclipse = {
		downloadSources = true,
	},
	maven = {
		downloadSources = true,
	},
	completion = {
		favoriteStaticMembers = {
			"org.hamcrest.MatcherAssert.assertThat",
			"org.hamcrest.Matchers.*",
			"org.hamcrest.CoreMatchers.*",
			"org.junit.jupiter.api.Assertions.*",
			"java.util.Objects.requireNonNull",
			"java.util.Objects.requireNonNullElse",
			"org.mockito.Mockito.*",
		},
		filteredTypes = {
			"com.sun.*",
			"io.micrometer.shaded.*",
			"java.awt.*",
			"jdk.*",
			"sun.*",
		},
	},
	sources = {
		organizeImports = {
			starThreshold = 9999,
			staticStarThreshold = 9999,
		},
	},
	codeGeneration = {
		toString = {
			template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}",
		},
		hashCodeEquals = {
			useJava7Objects = true,
		},
		useBlocks = true,
	},
	configuration = {
		runtimes = RUNTIMES[vim.loop.os_uname().sysname],
	},
	format = {
		enabled = true,
	},
	inlayHints = require("core.lsp").default.get_inlayhints(),
}
