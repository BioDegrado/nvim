local utilsos = require("utils.os")

local root_dir = vim.fs.find(
	{ "angular.json", "ionic.config.json" },
	{ upward = true, stop = vim.env.HOME }
)[1]

root_dir = vim.fs.dirname(root_dir) or ""
local node_modules = root_dir .. "/node_modules"
local glob_modules = vim.env.HOME
	.. "/.config/nvm/versions/node/v21.4.0/lib/node_modules"
local cmd = {
	utilsos.is_windows() and "ngserver.cmd" or "ngserver",
	"--tsProbeLocations",
	node_modules,
	"--ngProbeLocations",
	node_modules,
	"--includeCompletionsWithSnippetText",
	"--includeAutomaticOptionalChainCompletions",
	"--stdio",
}
return {
	cmd = cmd,
	filetypes = { "typescript", "angular", "typescriptreact", "typescript.tsx" },
	on_new_config = function(new_config) new_config.cmd = cmd end,
	root_dir = function() return root_dir end,
}
