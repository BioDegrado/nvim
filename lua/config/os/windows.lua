vim.g.fsync = false
vim.o.shellslash = true

-- Shell for toggleterm
local bash = "C:/Program Files/Git/bin/bash.exe"
if require("utils.fs").file_exists(bash) then
	-- if false then
	vim.opt.shell = bash
	vim.opt.shellcmdflag = "-s"
else
	vim.opt.shell = vim.fn.executable("pwsh") == 1 and "pwsh" or "powershell"
	vim.opt.shellcmdflag =
		"-NoLogo -NoProfile -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;"
	vim.opt.shellredir = "-RedirectStandardOutput %s -NoNewWindow -Wait"
	vim.opt.shellpipe = "2>&1 | Out-File -Encoding UTF8 %s; exit $LastExitCode"
	vim.opt.shellquote = ""
	vim.opt.shellxquote = ""
end
