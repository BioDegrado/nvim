vim.opt.termguicolors = true

-- Install lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
local lazyrepo = "https://github.com/folke/lazy.nvim.git"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"--branch=stable",
		lazyrepo,
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	spec = {
		--- Core plugins that are essential for the intended experience of the
		--- neovim setup.
		{ import = "plugins.core" },
		{ import = "plugins.debug" },
		{ import = "plugins.general" },
		{ import = "plugins.completion.nvim-cmp", enabled = false },
		{ import = "plugins.completion.blink", enabled = true },
		{ import = "plugins.lsp" },
		{ import = "plugins.ui" },
		--- Copilot integration.
		--- Decomment it to add support for copilot
		-- { import = "plugins.extra.ai.copilot" },
	},
	-- install = { colorscheme = "catppuccin" },
	checker = { enabled = true },
	ui = {
		border = "rounded",
	},
})
