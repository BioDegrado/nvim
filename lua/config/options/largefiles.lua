-- NOTE: Treesitter is automatically disabled on large files
local M = {}

local max_line_count = 100000
local max_line_length = 10000

local plugin_manager = require("core.plugin")

M.is_big_file = function(bufnr)
	local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, true)
	local bmax_line_length = 0
	for _, line in pairs(lines) do
		bmax_line_length = math.max(#line, bmax_line_length)
	end
	local is_large = #lines > max_line_count
	local is_long = bmax_line_length > max_line_length
	if is_large then
		vim.notify("Disabling features due to file being too big!")
	end
	if is_long then
		vim.notify(
			"Disabling heavy features due to file having lines that are too long!"
		)
	end
	return is_large or is_long
end

M.disable_features = function(bufnr)
	vim.notify("Disabling heavy features", vim.log.levels.INFO)
	plugin_manager.when_ready("ufo", function(ufo)
		vim.notify("Disabling ufo")
		ufo.detach(bufnr)
	end)

	plugin_manager.when_ready("indent_blankline.commands", function(ibl)
		vim.notify("Disabling indent blankline")
		ibl.disable()
	end)

	vim.notify("Disabling heavy options")
	vim.opt_local.foldmethod = "manual"
	vim.opt_local.breakindent = false
	vim.opt.copyindent = false
	vim.opt.preserveindent = false
	-- vim.opt_local.undolevels = -1
	-- vim.opt_local.undoreload = 0
	vim.opt_local.list = false
end
return M
