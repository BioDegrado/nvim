local core = require("core")

core.print_on_error(core.safe_require, "config.options.optimizations")
core.print_on_error(core.safe_require, "config.options.generic")
core.print_on_error(core.safe_require, "config.options.gui")
