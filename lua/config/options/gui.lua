vim.opt.termguicolors = true

vim.opt.laststatus = 3

vim.opt.cmdheight = 0
vim.opt.foldcolumn = "auto"
vim.opt.wrap = true
vim.opt.showmode = false

vim.opt.scrolloff = 1000
vim.opt.sidescrolloff = 8

vim.opt.selection = "old"

vim.opt.cursorline = true
vim.opt.signcolumn = "yes"
vim.opt.showtabline = 0 -- Disables the bufferline on top
vim.opt.showmatch = true

-- Numbers
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.numberwidth = 2

vim.opt.splitbelow = true
vim.opt.splitright = true

-- Indent
vim.opt.smartindent = false
vim.opt.breakindent = true
vim.opt.copyindent = true
vim.opt.preserveindent = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.linebreak = true
vim.opt.expandtab = false

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
	border = "single",
})
