vim.opt.writebackup = false
vim.opt.ttyfast = true
vim.opt.swapfile = false
vim.opt.updatetime = 300
vim.opt.timeoutlen = 400
vim.opt.inccommand = "nosplit"
vim.opt.lazyredraw = false -- Maybe... Incompatibility with Noice

--don't write to the ShaDa file on startup
vim.opt.shadafile = "NONE"

-- Disable useless builtin plugins
local disabled_built_ins = {
	"netrw",
	"netrwPlugin",
	"netrwSettings",
	"netrwFileHandlers",
	"gzip",
	"zip",
	"zipPlugin",
	"tar",
	"tarPlugin",
	"getscript",
	"getscriptPlugin",
	"vimball",
	"vimballPlugin",
	"2html_plugin",
	"logipat",
	"rrhelper",
	"spellfile_plugin",
	"matchit",
}

for _, plugin in pairs(disabled_built_ins) do
	vim.g["loaded_" .. plugin] = 1
end

vim.g["loaded_python3_provider"] = 0
vim.g["loaded_python_provider"] = 0
vim.g["loaded_node_provider"] = 0
vim.g["loaded_perl_provider"] = 0
vim.g["loaded_ruby_provider"] = 0
