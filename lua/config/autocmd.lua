local autocmd = vim.api.nvim_create_autocmd

local autogroup_config = { clear = true }
local autogroup = function(name, opts)
	vim.api.nvim_create_augroup(name, opts or autogroup_config)
end

local general = autogroup("general")
local frontend = autogroup("frontend")

autocmd(
	"BufReadPost",
	{ pattern = "*.conf", command = "setlocal ft=conf", group = general }
)

autocmd({ "FocusGained", "TermClose", "TermLeave" }, {
	group = autogroup("checktime"),
	callback = function()
		if vim.o.buftype ~= "nofile" then
			vim.cmd("checktime")
		end
	end,
})

autocmd("BufWritePre", {
	group = autogroup("autodir"),
	callback = function(event)
		if event.match:match("^%w%w+://") then
			return
		end
		local file = vim.uv.fs_realpath(event.match) or event.match
		vim.fn.mkdir(vim.fn.fnamemodify(file, ":p:h"), "p")
	end,
})

autocmd("BufRead", {
	group = autogroup("bigfile"),
	callback = function(args)
		local largefiles = require("config.options.largefiles")
		if largefiles.is_big_file(args.buf) then
			vim.notify("Disabling heavy features", vim.log.levels.INFO)
			largefiles.disable_features(args.buf)
		end
	end,
	desc = "Auto disables features on large files.",
})

-- Disable diagnostics in node_modules (0 is current buffer only)
autocmd({ "BufRead", "BufNewFile" }, {
	group = frontend,
	pattern = "*/node_modules/*",
	command = "lua vim.diagnostic.disable(0)",
})

-- Enable spell checking for certain file types
autocmd({ "BufRead", "BufNewFile" }, {
	group = general,
	pattern = { "*.txt", "*.md", "*.tex" },
	command = "setlocal spell",
})
