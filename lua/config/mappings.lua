-- Here are all the basic mappings that  are not related to plugins and other additional components.
local M = {}

local core = require("core")
-- local ok, wk = core.safe_require("which-key")
-- if not ok then
-- 	-- If which key is not available, we create a dummy object
-- 	-- that will not do anything.
-- 	wk = { register = function() end }
-- end

-- NOTE: The mappings here should never have a direct call to a function.
-- Instead is better if the call is wrapped.
-- For keybindings of plugins is better to use the lazy option "keys".
local vim = vim
local set_keymap = vim.keymap.set
local normal = "n"
local insert = "i"
local visual_select = "v"
local silent = { silent = true }
local expression = { silent = true, expr = true }

-- Disable annoying keys
set_keymap({ normal, visual_select }, "<Space>", "<Nop>", silent)
set_keymap({ normal, visual_select }, "<F1>", "<Nop>", silent)

-- Utils
-- set_keymap(normal, "s", "sa")
set_keymap(normal, "n", "nzz")
set_keymap(normal, "N", "Nzz")
set_keymap(normal, ",", "@q")
set_keymap(insert, ".", ".<C-g>u")
set_keymap({ normal, visual_select }, ";", ":")

-- Movement
set_keymap(normal, "j", "v:count == 0 ? 'gj' : 'j'", expression)
set_keymap(normal, "k", "v:count == 0 ? 'gk' : 'k'", expression)

-- Exiting insert mode with shift errors
set_keymap(insert, "jk", "<Esc>")
set_keymap(insert, "jK", "<Esc>")
set_keymap(insert, "Jk", "<Esc>")
set_keymap(insert, "JK", "<Esc>")
set_keymap(insert, "kj", "<Esc>")
set_keymap(insert, "kJ", "<Esc>")
set_keymap(insert, "Kj", "<Esc>")
set_keymap(insert, "KJ", "<Esc>")

local function toggle_fullscreen()
	local fullscreen = false
	return function()
		if not fullscreen then
			vim.print("going full")
			vim.cmd("tabe %")
			fullscreen = true
		else
			vim.print("going normal")
			vim.cmd("q")
			fullscreen = false
		end
	end
end
set_keymap(
	normal,
	"tf",
	toggle_fullscreen(),
	{ desc = "Toggle fullscreen mode for a single buffer" }
)

-- Windows navigation keymaps
-- wk.register({ ["leader"] = { w = { name = "+window management" } } })
set_keymap(normal, "<leader>wh", "<C-w>h")
set_keymap(normal, "<leader>wj", "<C-w>j")
set_keymap(normal, "<leader>wk", "<C-w>k")
set_keymap(normal, "<leader>wl", "<C-w>l")
set_keymap(normal, "<leader>w=", "<C-w>=")
set_keymap(normal, "<leader>ww", function()
	local buffers = require("utils.buffers")
	if not buffers.current_buf_is({ "terminal", "nofile" }) then
		vim.cmd("vs")
	end
end, { desc = "Open a vertical split" })
set_keymap(
	normal,
	"<leader>wo",
	"<cmd>split<CR>",
	{ desc = "Open a horizontal split" }
)
set_keymap(normal, "<leader>wd", "<cmd>tabclose<CR>", { desc = "Close a tab" })
set_keymap(
	normal,
	"<leader>wn",
	"<cmd>tabnext<CR>",
	{ desc = "Go to next tab" }
)
set_keymap(
	normal,
	"<leader>wp",
	"<cmd>tabprevious<CR>",
	{ desc = "Go to prev tab" }
)

-- set_keymap(normal, "<M-h>", "<cmd>vertical resize -2<CR>")
-- set_keymap(normal, "<M-j>", "<cmd>resize -2<CR>")
-- set_keymap(normal, "<M-k>", "<cmd>resize +2<CR>")
-- set_keymap(normal, "<M-l>", "<cmd>vertical resize +2<CR>")

-- set_keymap(normal, "<C-h>", "<cmd>vertical resize -2<CR>")
-- set_keymap(normal, "<C-j>", "<cmd>resize -2<CR>")
-- set_keymap(normal, "<C-k>", "<cmd>resize +2<CR>")
-- set_keymap(normal, "<C-l>", "<cmd>vertical resize +2<CR>")

-- Cycle buffer keymaps
-- set_keymap(normal, "<S-j>", "<cmd>BufferLineCycleNext<CR>")
-- set_keymap(normal, "<S-k>", "<cmd>BufferLineCyclePrev<CR>")

-- Git keymaps
set_keymap(
	normal,
	"<Leader>va",
	"<cmd>Git add .<CR>",
	{ desc = "Stage every file" }
)
set_keymap(normal, "<Leader>vC", "<cmd>Git commit<CR>", { desc = "Commit" })
set_keymap(
	normal,
	"<Leader>vp",
	"<cmd>Git push<CR>",
	{ desc = "Push to default remote" }
)
set_keymap(normal, "<Leader>vv", function()
	local commit_msg = vim.fn.input("Commit message:")
	if commit_msg == "" then
		print("Operation cancelled!")
		return
	end
	vim.cmd("Git add .")
	vim.cmd(string.format("Git commit -m '%s'", commit_msg))
	vim.cmd(string.format("Git push"))
end, { desc = "Quick add all -> commit -> push." })

set_keymap(
	normal,
	"<Leader>vb",
	function() require("gitsigns").toggle_current_line_blame() end,
	{ desc = "Toggle git blame on current line" }
)
set_keymap(
	normal,
	"<Leader>vB",
	function() require("gitsigns").blame_line({ full = true }) end,
	{ desc = [[Pop up a window showing "git blame" on current line]] }
)
set_keymap(
	normal,
	"<Leader>vh",
	function() require("gitsigns").preview_hunk() end,
	{ desc = "Preview current line hunk" }
)
set_keymap(
	normal,
	"<Leader>vd",
	function() vim.cmd("DiffviewOpen") end,
	{ desc = "View git diffs until index" }
)

set_keymap(
	normal,
	"<Leader>vf",
	function() vim.cmd("DiffviewFileHistory %") end,
	{ desc = "View git diffs of file" }
)

set_keymap(
	visual_select,
	"<Leader>vd",
	function() vim.cmd("'<,'>DiffviewFileHistory") end,
	{ desc = "View git diffs of range" }
)

set_keymap(
	normal,
	"]q",
	function() vim.cmd("cnext") end,
	{ desc = "Go to next quickfix item" }
)
set_keymap(
	normal,
	"[q",
	function() vim.cmd("cprev") end,
	{ desc = "Go to prev quickfix item" }
)

set_keymap(
	normal,
	"]h",
	function() require("gitsigns").next_hunk() end,
	{ desc = "Go to next hunk" }
)

set_keymap(
	normal,
	"[h",
	function() require("gitsigns").prev_hunk() end,
	{ desc = "Go to previous hunk" }
)

set_keymap(
	normal,
	"<Leader>a",
	vim.lsp.buf.code_action,
	{ desc = "Choose code action" }
)
set_keymap(
	normal,
	"<Leader>x",
	"<cmd>!chmod u+x %<CR>",
	{ desc = "Makes the current file executable" }
)
set_keymap(
	normal,
	"<Leader>q",
	function() require("utils.buffers").smart_close("bd", 0, false) end
)
set_keymap(normal, "<Leader>r", function()
	vim.lsp.buf.rename(nil, {
		filter = function(server)
			if server.name == "ts_ls" or server.name == "typescript-tools" then
				if vim.fs.root(0, "angular.json") then
					print("Server deactivated: " .. server.name)
					return false
				end
			end
			print("Server active: " .. server.name)
			return true
		end,
	})
end, { desc = "Rename object under cursor." })

set_keymap(visual_select, "J", ":m '>+1<CR>gv=gv")
set_keymap(visual_select, "K", ":m '<-2<CR>gv=gv")
set_keymap(visual_select, ">", ">gv")
set_keymap(visual_select, "<", "<gv")

set_keymap(
	normal,
	"]t",
	function() require("todo-comments").jump_next() end,
	{ desc = "Next todo comment" }
)

set_keymap(
	normal,
	"[t",
	function() require("todo-comments").jump_prev() end,
	{ desc = "Previous todo comment" }
)
set_keymap(
	normal,
	"<lodaer>ft",
	"<cmd>TodoTelescope<cr>",
	{ desc = "Find todo comments." }
)

local function toggle_hint()
	vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
end
--LSP
M.lsp = function()
	set_keymap(
		normal,
		"<Leader>h",
		vim.lsp.buf.hover,
		{ silent = true, desc = "Show infos under cursor." }
	)

	set_keymap(
		normal,
		"]E",
		function() vim.diagnostic.goto_next() end,
		{ desc = "go to next diagnostic" }
	)
	set_keymap(
		normal,
		"[E",
		function() vim.diagnostic.goto_prev() end,
		{ desc = "go to prev diagnostic" }
	)
	set_keymap(normal, "]e", function()
		local error = { severity = vim.diagnostic.severity.ERROR }
		if not vim.diagnostic.get_next(error) then
			vim.diagnostic.goto_next()
		end
		vim.diagnostic.goto_next(error)
	end, { desc = "go to next Error" })
	set_keymap(normal, "[e", function()
		local error = { severity = vim.diagnostic.severity.ERROR }
		if not vim.diagnostic.get_prev(error) then
			vim.diagnostic.goto_prev()
		end
		vim.diagnostic.goto_prev(error)
	end, { desc = "go to prev Error" })

	set_keymap(
		normal,
		"<Leader>e",
		vim.diagnostic.open_float,
		{ silent = true, desc = "Show diagnostics under cursor." }
	)

	local lsp = vim.lsp
	set_keymap(normal, "gd", function() lsp.buf.definition() end, silent)
	set_keymap(normal, "gD", function() lsp.buf.declaration() end, silent)
	set_keymap(normal, "gt", function() lsp.buf.type_definition() end, silent)
	set_keymap(
		normal,
		"gr",
		function() lsp.buf.references({ includeDeclaration = false }) end,
		silent
	)
	set_keymap(normal, "gi", function() lsp.buf.implementation() end, silent)
	set_keymap(
		normal,
		"<Leader>i",
		toggle_hint,
		{ silent = true, desc = "Toggle inlay Hints" }
	)
end

--- Mappings for nvim_tree
M.nvim_tree = function(bufnr, api)
	local function options(desc)
		return {
			desc = "NvimTree: " .. desc,
			buffer = bufnr,
			noremap = true,
			silent = true,
			nowait = true,
		}
	end

	vim.keymap.set("n", "u", api.tree.collapse_all, options("Collapse folders"))

	vim.keymap.set(
		"n",
		"J",
		api.node.navigate.sibling.next,
		options("Go to next sibling")
	)
	vim.keymap.set(
		"n",
		"K",
		api.node.navigate.sibling.prev,
		options("Go to prev sibling")
	)

	vim.keymap.set("n", "H", api.node.navigate.parent, options("Go to parent"))
	vim.keymap.set(
		"n",
		"h",
		api.node.navigate.parent_close,
		options("Go to parent and close")
	)
	vim.keymap.set("n", "l", api.node.open.edit, options("Open folder"))
	vim.keymap.set("n", ".", api.node.run.cmd, options("Run Command"))
	vim.keymap.set("n", "a", api.fs.create, options("Create"))
	vim.keymap.set("n", "c", api.fs.copy.node, options("Copy"))
	vim.keymap.set("n", "d", api.fs.remove, options("Delete"))
	vim.keymap.set("n", "r", api.fs.rename, options("Rename"))
	vim.keymap.set("n", "S", api.tree.search_node, options("Search"))
	vim.keymap.set("n", "x", api.fs.cut, options("Cut"))
	vim.keymap.set("n", "y", api.fs.copy.filename, options("Copy Name"))
	-- vim.keymap.set("n", "I", api., options("Copy Name"))
	vim.keymap.set(
		"n",
		"Y",
		api.fs.copy.relative_path,
		options("Copy Relative Path")
	)
	vim.keymap.set("n", "p", api.fs.paste, options("Paste"))
	vim.keymap.set("n", "o", api.node.open.edit, options("Open"))
	vim.keymap.set(
		"n",
		"O",
		api.node.open.vertical,
		options("Open in new vertical buffer")
	)
	vim.keymap.set("n", "-", api.tree.change_root_to_node, options("CD"))
	vim.keymap.set("n", "<CR>", api.node.open.edit, options("Open"))
end

set_keymap(normal, "<Leader>dt", function()
	local path = vim.fn.expand("%:p:h")
	os.execute("$TERMINAL start --cwd '" .. path .. "' &")
end)

-- TODO: Put in a better place
--- ANGULAR
local function get_options(path, config)
	local endswith = {}
	for _, e in pairs(config) do
		table.insert(endswith, e.ext)
	end

	local matches = require("utils.fs").find_files_ends_with(endswith, path)
	local res = {}
	for _, match in pairs(matches) do
		config[match.index].file = match.file
		res[#res + 1] = config[match.index]
	end
	return res
end

vim.keymap.set("n", "<leader>fs", function()
	local file = vim.fn.expand("%:h")

	local config = get_options(file, {
		{ ext = "html", opt = nil },
		{ ext = "ts", opt = nil },
		{ ext = "scss", opt = nil },
		{ ext = "spec.ts", opt = "s&pec.ts" },
		{ ext = "module.ts", opt = nil },
		{ ext = "__mocks__/(.*).ts", opt = nil },
	})

	local options = {}
	for _, val in ipairs(config) do
		options[#options + 1] = val.opt or val.ext
	end

	options[#options + 1] = "Cancel"

	local message = string.format("Pick a format for %q:", file)
	local choice = vim.fn.confirm(message, table.concat(options, "\n"))
	if choice == 0 or choice == #options then
		return
	end

	vim.cmd("e " .. config[choice].file)
end, { desc = "Open one of the files of the angular project" })

return M
