return {
	"catppuccin/nvim",
	name = "catppuccin",
	priority = 1000, -- For loading before any other plugins
	init = function() vim.cmd("colorscheme catppuccin") end,
	config = true,
	opts = {
		flavour = "mocha",
		transparent_background = true,
		dim_inactive = {
			enabled = true,
			shade = "dark",
			percentage = 0.0,
		},
		termcolors = true,
		integrations = {
			mason = true,
			treesitter = true,
			native_lsp = {
				enabled = true,
				virtual_text = {
					errors = { "italic" },
					hints = { "italic" },
					warnings = { "italic" },
					information = { "italic" },
				},
				underlines = {
					errors = { "undercurl" },
					hints = { "undercurl" },
					warnings = { "undercurl" },
					information = { "undercurl" },
				},
			},
			cmp = true,
			nvimtree = true,
			which_key = true,
			indent_blankline = {
				enabled = true,
				colored_indent_levels = true,
			},
			gitsigns = true,
			markdown = true,
			noice = true,
		},
		custom_highlights = function(colors)
			return {
				CursorLine = { bg = colors.crust },
				ColorColumn = { bg = colors.surface1 },
			}
		end,
	},
}
