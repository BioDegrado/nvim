return {
	"akinsho/bufferline.nvim",
	enabled = false,
	dependencies = { "kyazdani42/nvim-web-devicons" },
	opts = function()
		local mocha = require("catppuccin.palettes").get_palette("mocha")
		local active_color = "#00ff00"
		local bufferline = require("bufferline")
		return {
			highlights = require("catppuccin.groups.integrations.bufferline").get({
				styles = { "italic", "bold" },
				custom = {
					all = {
						background = { fg = mocha.text },
						buffer_selected = {
							fg = active_color,
							style = { "bold" },
						},
						hint_selected = {
							fg = active_color,
						},
						error_selected = {
							fg = active_color,
						},
						warning_selected = {
							fg = active_color,
						},
					},
				},
			}),
			options = {
				style_preset = bufferline.style_preset.minimal,
				close_command = function(bufnr)
					require("utils.buffers").smart_close("bd", bufnr, false)
				end,
				show_buffer_close_icons = false,
				offsets = { { filetype = "NeoTree", text = "", padding = 1 } },
				diagnostics = "nvim_lsp",
				indicator = {
					icon = "",
					style = "none",
				},
				diagnostics_indicator = function(_, _, diagnostics)
					local str = ""
					for err, num in pairs(diagnostics) do
						local sym = err == "error" and " "
							or (err == "warning" and " " or "")
						str = str .. " " .. num .. sym
					end
					return str
				end,
				separator_style = { "", "" },
				themable = true,
			},
		}
	end,
}
