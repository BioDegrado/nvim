return {
	"folke/todo-comments.nvim",
	config = true,
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope.nvim",
	},
	event = { "VeryLazy" },
}
