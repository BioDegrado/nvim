local vim = vim
local theme = {
	aqua = "#7AB0DF",
	bg = "#111113",
	blue = "#84A0C6",
	cyan = "#70C0BA",
	darkred = "#FB7373",
	fg = "#C7C7CA",
	gray = "#29292d",
	green = "#78B892",
	lime = "#54CED6",
	orange = "#FFD064",
	pink = "#D997C8",
	purple = "#C397D8",
	red = "#F87070",
	yellow = "#FFE59E",
}

local function get_theme()
	local catppuccin_mocha = require("lualine.themes.catppuccin-mocha")
	catppuccin_mocha.normal.a.bg = theme.lime
	catppuccin_mocha.insert.a.bg = theme.green
	catppuccin_mocha.visual.a.bg = theme.purple
	catppuccin_mocha.command.a.bg = theme.orange
	return catppuccin_mocha
end

local function scrollbar(str)
	if str == "Top" then
		return "__"
	elseif str == "Bot" then
		return "██"
	else
		local level = str:sub(0, #str - 2)
		local idx = math.ceil(tonumber(level) / 10)
		local levels = {
			"__",
			"▁▁",
			"▂▂",
			"▃▃",
			"▄▄",
			"▅▅",
			"▆▆",
			"▇▇",
			"██",
			"██",
		}
		return levels[idx]
	end
end
local function get_line_length()
	local line = -1
	local line_length = 0
	return function()
		if vim.api.nvim_get_current_line() ~= line then
			line_length =
				vim.fn.strdisplaywidth(vim.api.nvim_get_current_line())
		end
		return "L:" .. line_length
	end
end

local function visual_selection_range()
	local _, csrow, cscol, _ = unpack(vim.fn.getpos("v"))
	local _, cerow, cecol, _ = unpack(vim.fn.getpos("."))
	if csrow < cerow or (csrow == cerow and cscol <= cecol) then
		return csrow - 1, cscol - 1, cerow - 1, cecol
	else
		return cerow - 1, cecol - 1, csrow - 1, cscol
	end
end

local function word_count()
	local count = 0
	local lines = {}
	local mode = vim.api.nvim_get_mode().mode
	if mode == "v" or mode == "V" then
		local srow, scol, erow, ecol = visual_selection_range()
		lines = vim.api.nvim_buf_get_text(0, srow, scol, erow, ecol, {})
	else
		lines = vim.api.nvim_buf_get_lines(0, 0, -1, true)
	end
	local text = table.concat(lines, "\n")
	for _ in string.gmatch(text, "%S+") do
		count = count + 1
	end
	return "W:" .. tostring(count)
	-- return "W:" .. tostring(#words)
end

return {
	"nvim-lualine/lualine.nvim",
	opts = {
		options = {
			theme = get_theme(),
			component_separators = { left = "", right = "" },
			section_separators = { left = "", right = "" },
		},
		sections = {
			lualine_a = {
				{
					"mode",
					fmt = function(str)
						if str == "NORMAL" then
							return "󰜎"
						elseif str == "INSERT" then
							return "󱩼"
						elseif str == "VISUAL" then
							return "󰾂"
						elseif str == "COMMAND" then
							return ""
						else
							return str:sub(1, 1)
						end
					end,
					-- color = { bg = theme.lime },
					separator = { left = "", right = "" },
					-- padding = 1,
				},
				{
					function() return vim.fn.reg_recording() end,
					cond = function() return vim.fn.reg_recording() ~= "" end,
					fmt = function(str) return " " .. str end,
					color = { fg = theme.gray, bg = theme.red },
					separator = { left = "", right = "" },
					padding = 1,
				},
			},
			lualine_b = {
				{
					"filename",
					path = 1,
					symbols = {
						modified = "󰙏",
						readonly = "󰌾",
						newfile = "",
					},
					color = { fg = "", bg = "transparent" },
				},
				{
					"diff",
					color = { fg = "", bg = "transparent" },
				},
			},
			lualine_c = {
				{
					"branch",
					color = { bg = theme.yellow, fg = "black" },
					separator = { left = "", right = "" },
				},
				{
					"diagnostics",
					sources = { "nvim_workspace_diagnostic" },
					color = {},
					symbols = {
						error = " ",
						warn = " ",
						info = " ",
						hint = " ",
					},
				},
			},

			lualine_x = {
				"encoding",
				{
					"fileformat",
					color = { fg = "black", bg = theme.pink },
					separator = { left = "", right = "" },
					padding = { left = 0, right = 1 },
				},
				{
					"filetype",
					color = { bg = theme.bg },
					separator = { left = "", right = "" },
				},
			},
			lualine_y = {},
			lualine_z = {
				{
					get_line_length(),
					color = { fg = "white", bg = "transparent" },
				},
				{
					"progress",
					fmt = scrollbar,
					color = { fg = theme.darkred },
					separator = { left = "", right = "" },
				},
				{
					word_count,
					cond = function()
						local filetype = vim.bo.filetype
						return (
							filetype == "tex"
							or filetype == "markdown"
							or filetype == "markdown.pandoc"
						)
					end,
				},
			},
		},
	},
	dependencies = {
		{ "kyazdani42/nvim-web-devicons" },
	},
	event = { "BufReadPre", "BufNewFile" },
	config = true,
}
