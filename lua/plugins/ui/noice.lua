return {
	"folke/noice.nvim",
	dependencies = {
		"MunifTanjim/nui.nvim",
		"rcarriga/nvim-notify",
	},
	enabled = true,
	init = function() vim.o.shortmess = vim.o.shortmess .. "sWc" end,
	config = function(_, opts)
		require("notify").setup({
			background_colour = "#000000",
			stages = "fade",
			render = "compact",
		})
		require("noice").setup(opts)
		vim.keymap.set("n", "<Leader>n", "<cmd>Noice dismiss<CR>")
	end,
	opts = {
		lsp = {
			progress = {
				enabled = true,
			},
			override = {
				["vim.lsp.util.convert_input_to_markdown_lines"] = true,
				["vim.lsp.util.stylize_markdown"] = true,
				["cmp.entry.get_documentation"] = true,
			},
		},
		presets = {
			command_palette = true,
			long_message_to_split = true,
			inc_rename = false,
			lsp_doc_border = false,
		},
		routes = {
			{
				filter = {
					event = "msg_show",
					kind = "",
				},
				opts = { skip = true },
			},
		},
		views = {
			notify = {
				winhiglight = {
					NotifyBackground = "#000000",
				},
			},
			mini = {
				winhiglight = {},
			},
			hover = {
				border = { style = "rounded" },
				-- Positions the doc lower so that the line is not obfuscated
				position = { row = 2, col = 0 },
				size = { max_width = 80 },
			},
		},
	},
}
