local vim = vim

return {
	enabled = true,
	"kevinhwang91/nvim-ufo",
	dependencies = {
		"neovim/nvim-lspconfig",
		"kevinhwang91/promise-async",
		"nvim-treesitter/nvim-treesitter",
	},
	init = function()
		vim.o.foldcolumn = "1"
		vim.o.foldlevel = 99
		vim.o.foldlevelstart = 99
		vim.o.foldenable = true
		vim.o.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]
	end,
	opts = {
		close_fold_kinds_for_ft = { default = { "imports", "comment" } },
		provider_selector = function(_, filetype, _)
			-- The 'angular service' does not give a good folding experience.
			-- It's better to use treesitter for this.
			if filetype == "angular" then
				return "treesitter"
			end
			return { "lsp", "indent" }
		end,
	},
	event = { "BufRead" },
	config = function(_, opts)
		require("ufo").setup(opts)

		vim.keymap.set(
			"n",
			"zR",
			function() require("ufo").openAllFolds() end,
			{ desc = "Open all folds." }
		)
		vim.keymap.set(
			"n",
			"zM",
			function() require("ufo").closeAllFolds() end,
			{ desc = "Close all folds." }
		)
		vim.keymap.set(
			"n",
			"zr",
			function() require("ufo").openFoldsExceptKinds() end,
			{ desc = "Open folds except kinds." }
		)

		require("core.plugin").when_ready("ufo")
	end,
}
