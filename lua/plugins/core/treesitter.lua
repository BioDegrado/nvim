local LANGUAGES = {
	"c",
	"lua",
	"cpp",
	"python",
	"latex",
	"markdown",
	"bibtex",
	"bash",
	"html",
	"css",
	"scss",
	"diff",
	"json",
	"make",
	"vim",
	"markdown_inline",

	-- GAMEDEV
	"gdscript",
	"godot_resource",
	"gdshader",

	-- GOLANG
	"go",
	"gomod",
	"gosum",
	"gowork",

	"dart",

	-- WEBDEV
	"java",
	"sql",
	"angular",
	"javascript",
	"typescript",
}

local HIGHLIGHT = {
	enable = true,
	disable = function(_, bufnr)
		-- local is_big = require("config.options.largefiles").is_big_file(bufnr)
		-- if is_big then
		-- 	vim.notify(
		-- 		"Disabling Treesitter, file too big",
		-- 		vim.log.levels.INFO
		-- 	)
		-- end
		-- return is_big
	end,
	additional_vim_regex_highlighting = false,
}

local SELECT = {
	enable = true,
	lookahead = true,
	keymaps = {
		-- The rest are defined in various-text-objects plugin
		["af"] = "@function.outer",
		["if"] = "@function.inner",
		["ac"] = "@class.outer",
		["ic"] = "@class.inner",
		-- ["ac"] = "@conditional.outer",
		-- ["ic"] = "@conditional.inner",
		["al"] = "@loop.outer",
		["il"] = "@loop.inner",

		-- NOTE: Doesn't always catch the entire comment block
		-- ["ag"] = "@comment.outer",
		-- ["ig"] = "@comment.inner",

		["ap"] = "@parameter.outer",
		["ip"] = "@parameter.inner",
		["am"] = "@call.outer",
		["im"] = "@call.inner",
		["as"] = "@statement.outer",
		["is"] = "@statement.inner",
		["av"] = "@assignment.rhs",
		["iv"] = "@assignment.rhs",
	},
}

local MOVE = {
	enable = true,
	set_jumps = true, -- whether to set jumps in the jumplist
	goto_next_start = {
		["]f"] = "@function.outer",
		["]z"] = "@closedFold.outer",
		["]p"] = "@parameter.outer",
		["]c"] = "@class.outer",
	},
	goto_previous_start = {
		["[f"] = "@function.outer",
		["[p"] = "@parameter.outer",
		["[c"] = "@class.outer",
		["[z"] = "@closedFold.outer",
	},
	goto_next_end = {
		["]F"] = "@function.outer",
	},
	goto_previous_end = {
		["[F"] = "@function.outer",
	},
}

return {
	"nvim-treesitter/nvim-treesitter",
	dependencies = {
		{ "nvim-treesitter/nvim-treesitter-textobjects" },
		{
			"chrisgrieser/nvim-various-textobjs",
			config = true,
			-- opts = { useDefaultKeymaps = true },
		},
	},
	init = function(plugin)
		require("lazy.core.loader").add_to_rtp(plugin)
		require("nvim-treesitter.query_predicates")
	end,
	config = function(_, opts) require("nvim-treesitter.configs").setup(opts) end,
	opts = {
		ensure_installed = LANGUAGES,
		highlight = HIGHLIGHT,
		indent = { enable = true },
		textobjects = {
			select = SELECT,
			move = MOVE,
		},
	},
	priority = 1000,
	lazy = false,
	build = ":TSUpdate",
	event = { "BufReadPre", "BufNewFile" },
}
