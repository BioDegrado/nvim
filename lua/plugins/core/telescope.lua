local IGNORE_FILES = {
	"^local",
	"^target",
	"%.aux",
	"%.toc",
	"%.pdf",
	"%.out",
	"%.png",
	"%.jpg",
	"%.jpeg",
	"%.zip",
	"%.gz",
	"%.gif",
	"%.bak",
	"%.7z",
	"%.iso",
	"%.odt",
	"%.odg",
	"%.class",
	"%.jar",
	"%.dat",
	"%.dumpstream",
	"%.dump",
	".repro/*",
	-- "build/*",
	"custom-plugins/*",
	"node_modules/*",
	".git/*",
	".DS_Store",
	"platforms/*",
}

return {
	"nvim-telescope/telescope.nvim",
	dependencies = {
		"nvim-lua/popup.nvim",
		"nvim-lua/plenary.nvim",
		{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
	},
	config = function()
		local telescope = require("telescope")
		local actions = require("telescope.actions")
		telescope.setup({
			defaults = {
				path_display = {
					filename_first = {
						reverse_directories = true,
					},
				},
				get_selection_window = function()
					local get_buftype = require("utils.buffers").get_type
					local buf = vim.api.nvim_get_current_buf()
					if get_buftype(buf) ~= "terminal" then
						return 0
					end

					for _, win in ipairs(vim.api.nvim_list_wins()) do
						local wbuf = vim.api.nvim_win_get_buf(win)
						local is_loaded = vim.api.nvim_buf_is_loaded(wbuf)
						local is_terminal = get_buftype(wbuf) ~= "terminal"
						if is_loaded and is_terminal then
							return win
						end
					end
				end,
				-- sorting_strategy = "ascending",
				file_ignore_patterns = IGNORE_FILES,
				initial_mode = "insert",
				mappings = {
					i = {
						["<esc>"] = actions.close,
						["jk"] = actions.close,
						["kj"] = actions.close,
					},
				},
			},
			extensions = {
				fzf = {
					fuzzy = true,
					override_generic_sorter = true,
					override_file_sorter = true,
					case_mode = "smart_case",
					matcher = "fuzzy",
				},
			},
			set_env = { ["COLORTERM"] = "truecolor" },
		})
		telescope.load_extension("fzf")
	end,
	keys = {
		{
			"<Leader>ff",
			function()
				require("telescope.builtin").find_files({
					hidden = true,
				})
			end,
			desc = "Find files",
		},
		{
			"<Leader>fg",
			function() require("telescope.builtin").live_grep() end,
			desc = "Grep",
		},
		{
			"<Leader>fd",
			function() require("telescope.builtin").lsp_document_symbols() end,
			desc = "workspace symbols",
		},
		{
			"<Leader>fD",
			function()
				require("telescope.builtin").lsp_dynamic_workspace_symbols()
				vim.lsp.buf_request(0, "workspace/symbols", nil, function(err)
					if err then
						require("telescope.builtin").lsp_dynamic_workspace_symbols()
					else
						require("telescope.builtin").lsp_workspace_symbols()
					end
				end)
			end,
			desc = "workspace symbols",
		},
		{
			"<Leader><Space>",
			function() require("telescope.builtin").buffers() end,
			desc = "Buffers",
		},
		{
			"<Leader>fe",
			function()
				require("telescope.builtin").diagnostics({ severity = "ERROR" })
			end,
			desc = "Telescope find errors",
		},
		{
			"<Leader>fE",
			function() require("telescope.builtin").diagnostics() end,
			desc = "Telescope find diagnostics",
		},
		{
			"<Leader>fc",
			function() require("telescope.builtin").commands() end,
			desc = "Search commands",
		},
		{
			"<Leader>fh",
			function() require("telescope.builtin").help_tags() end,
			desc = "Help neovim",
		},
		{
			"<Leader>fk",
			function() require("telescope.builtin").keymaps() end,
			desc = "Help keymaps",
		},
		{
			"<Leader>vc",
			function() require("telescope.builtin").git_commits() end,
			desc = "Search git commits",
		},
		{
			"<Leader>vs",
			function() require("telescope.builtin").git_status() end,
			desc = "Search git status",
		},
	},
	cmd = { "Telescope" },
}
