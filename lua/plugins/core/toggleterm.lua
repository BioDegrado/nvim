local lazygit = nil
return {
	enabled = true,
	"akinsho/toggleterm.nvim",
	opts = {
		shell = vim.o.shell,
		on_open = function()
			vim.keymap.set("t", "<Esc>", "<C-\\><C-N>", {
				desc = "Exit insert mode from terminal",
				buffer = true,
			})
			vim.keymap.set("t", "jk", "<C-\\><C-N>", {
				desc = "Exit insert mode from terminal (rapid shortcut)",
				buffer = true,
			})
			vim.keymap.set("t", "<C-h>", "<C-\\><C-N><C-w>h", { buffer = true })
			vim.keymap.set("t", "<C-j>", "<C-\\><C-N><C-w>j", { buffer = true })
			vim.keymap.set("t", "<C-k>", "<C-\\><C-N><C-w>k", { buffer = true })
			vim.keymap.set("t", "<C-l>", "<C-\\><C-N><C-w>l", { buffer = true })
			vim.keymap.set(
				"t",
				"<M-h>",
				"<C-\\><C-N><cmd>vertical resize -2<CR>i",
				{ buffer = true }
			)
			vim.keymap.set(
				"t",
				"<M-j>",
				"<C-\\><C-N><cmd>resize -2<CR>i",
				{ buffer = true }
			)
			vim.keymap.set(
				"t",
				"<M-k>",
				"<C-\\><C-N><cmd>resize +2<CR>i",
				{ buffer = true }
			)
			vim.keymap.set(
				"t",
				"<M-l>",
				"<C-\\><C-N><cmd>vertical resize +2<CR>i",
				{ buffer = true }
			)
		end,
	},
	config = function(_, opts)
		require("toggleterm").setup(opts)
		lazygit = require("toggleterm.terminal").Terminal:new({
			cmd = "lazygit",
			hidden = true,
			direction = "float",
			float_opts = { border = "double" },
			close_on_exit = true,
			on_open = function() vim.cmd("startinsert!") end,
		})
	end,
	keys = {
		{
			"<Leader>s",
			function() require("toggleterm").toggle() end,
			mode = "n",
			desc = "Toggle terminal",
		},
		{
			"<Leader>vl",
			function()
				if lazygit ~= nil then
					lazygit:toggle()
				end
			end,
			mode = "n",
			desc = "Toggle LazyGit",
		},
	},
}
