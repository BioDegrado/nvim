return {
	"rcarriga/nvim-dap-ui",
	opts = {
		layouts = {
			{
				elements = {
					{
						id = "repl",
						size = 0.5,
					},
					{
						id = "console",
						size = 0.5,
					},
				},
				position = "bottom",
				size = 20,
			},
			{
				elements = {
					{
						id = "scopes",
						size = 0.25,
					},
					{
						id = "breakpoints",
						size = 0.25,
					},
					{
						id = "stacks",
						size = 0.25,
					},
					{
						id = "watches",
						size = 0.25,
					},
				},
				position = "right",
				size = 40,
			},
		},
		render = {
			max_value_lines = 10000,
		},
	},
	config = function(_, opts)
		local dapui = require("dapui")
		-- dap.listeners.after.event_initialized.dapui_config = dapui.open
		dapui.setup(opts)
	end,
	keys = {
		{
			"<Leader>du",
			function() require("dapui").toggle({ reset = true }) end,
			desc = "Toggle DAP UI",
		},
		{
			"<Leader>ds",
			function()
				local widgets = require("dap.ui.widgets")
				local sidebar = widgets.sidebar(widgets.scopes)
				sidebar.open()
			end,
			desc = "Open debug sidebar",
		},
	},
}
