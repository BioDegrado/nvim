return {
	"nvim-neotest/neotest",
	dependencies = {
		-- This is necessary, so the plugins runs after treesitter
		"nvim-neotest/nvim-nio",
		"nvim-treesitter/nvim-treesitter",
		"nvim-lua/plenary.nvim",

		"antoinemadec/FixCursorHold.nvim",
		-- Adapters
		"nvim-neotest/neotest-jest",
		"nvim-neotest/neotest-go",
		"rcasia/neotest-java",
	},
	config = function()
		require("neotest").setup({
			adapters = {
				require("neotest-jest")({
					jestCommand = "npm test --",
					jestConfigFile = "custom.jest.config.ts",
					env = { CI = true },
					cwd = function() return vim.fn.getcwd() end,
				}),
				require("neotest-java"),
			},
			-- default_strategy = "dap",
			discovery = {
				enabled = false,
			},
		})
	end,
	keys = {
		{
			"<Leader>tu",
			function()
				local neotest = require("neotest")
				neotest.summary.toggle()
				neotest.output_panel.toggle()
			end,
			{ desc = "Toggle testing interface" },
		},
		{
			"<Leader>tt",
			function() require("neotest").run.run() end,
			{ desc = "Testing interface" },
		},
		{
			"<Leader>tl",
			function() require("neotest").run.run_last() end,
			{ desc = "Testing last test" },
		},
	},
}
