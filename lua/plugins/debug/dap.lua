return {
	"mfussenegger/nvim-dap",
	dependencies = {
		"theHamsta/nvim-dap-virtual-text",
		"mfussenegger/nvim-dap-python",
		"rcarriga/nvim-dap-ui",
	},
	config = function()
		vim.fn.sign_define("DapBreakpoint", {
			text = "🟥",
			texthl = "DapUIBreakpointsInfo",
			linehl = "",
			numhl = "",
		})
		vim.fn.sign_define("DapBreakpointCondition", {
			text = "🟥",
			texthl = "DapUIBreakpointsInfo",
			linehl = "",
			numhl = "",
		})
		vim.fn.sign_define(
			"DapBreakpointRejected",
			{ text = "🟦", texthl = "", linehl = "", numhl = "" }
		)
		vim.fn.sign_define(
			"DapStopped",
			{ text = "⭐️", texthl = "DapUIStop", linehl = "", numhl = "" }
		)
		local dap = require("dap")
		dap.configurations.java = {
			{
				name = "Debug (Attach) - Remote",
				type = "java",
				request = "attach",
				hostName = "127.0.0.1",
				port = 5005,
			},
			{
				name = "Debug Non-Project class",
				type = "java",
				request = "launch",
				program = "${file}",
			},
		}
		dap.adapters.firefox = {
			type = "executable",
			command = "node",
			args = {
				vim.fn.stdpath("data")
					.. "/mason/packages/"
					.. "firefox-debug-adapter/dist/adapter.bundle.js",
			},
		}
		dap.adapters.chrome = {
			type = "executable",
			command = "node",
			args = {
				vim.fn.stdpath("data")
					.. "/mason/packages/"
					.. "chrome-debug-adapter/out/src/chromeDebug.js",
			},
		}

		-- dap.configurations.typescript = {
		-- 	{
		-- 		name = "Debug with Chrome",
		-- 		type = "chrome",
		-- 		request = "launch",
		-- 		reAttach = true,
		-- 		url = "http://localhost:3000",
		-- 		webRoot = "${workspaceFolder}",
		-- 		chromeExecutable = "/c/Program Files/Google/Chrome/Application/chrome.exe",
		-- 	},
		-- 	{
		-- 		name = "Debug with Firefox",
		-- 		type = "firefox",
		-- 		request = "launch",
		-- 		reAttach = true,
		-- 		url = "http://localhost:8100",
		-- 		webRoot = "${workspaceFolder}",
		-- 		-- firefoxExecutable = "/c/Program Files/Mozilla Firefox/firefox.exe",
		-- 	},
		-- }
	end,
	keys = {
		{
			"<Leader>ddd",
			function() require("dap").continue() end,
			desc = "Go to next step DAP",
		},
		{
			"<Leader>dj",
			function() require("dap").step_over() end,
			desc = "Go to next step DAP (Comfy key)",
		},
		{
			"<Leader>dk",
			function() require("dap").step_back() end,
			desc = "Step back",
		},
		{
			"<Leader>dl",
			function() require("dap").step_into() end,
			desc = "Step into",
		},
		{
			"<Leader>dh",
			function() require("dap").step_out() end,
			desc = "Step out of",
		},
		{
			"<Leader>dr",
			function() require("dap").repl.open() end,
			desc = "Open REPL for debuging",
		},
		{
			"<Leader>b",
			function() require("dap").toggle_breakpoint() end,
			desc = "Toggle breakpoint",
		},
		{
			"<Leader>B",
			function()
				require("dap").toggle_breakpoint(
					vim.fn.input("Breakpoint condition: ")
				)
			end,
			desc = "Toggle conditional breakpoint",
		},
		{
			"<Leader>dl",
			function() require("dap.ext.vscode").load_launchjs() end,
			desc = "Load launchjs for loading tests",
		},
		{
			"<Leader>ddl",
			function() require("dap").run_last() end,
			desc = "Run last test",
		},
		{
			"<Leader>dx",
			function() require("dap").terminate() end,
			desc = "Terminte debug session",
		},
	},
}
