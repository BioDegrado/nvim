return {
	"leoluz/nvim-dap-go",
	dependencies = {
		"mfussenegger/nvim-dap",
	},
	config = function()
		require("dap-go").setup()
		vim.keymap.set(
			"n",
			"<Leader>ddt",
			function() require("dap-go").debug_test() end,
			{ desc = "Debug with test function" }
		)
		vim.keymap.set(
			"n",
			"<Leader>ddT",
			function() require("dap-go").debug_test() end,
			{ desc = "Debug with test function" }
		)
	end,
	ft = "go",
}
