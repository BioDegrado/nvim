local function mapping_config()
	local cmp = require("cmp")
	return {
		["<Up>"] = cmp.mapping.scroll_docs(-4),
		["<Down>"] = cmp.mapping.scroll_docs(4),
		["<Right>"] = cmp.mapping.select_next_item(),
		["<Left>"] = cmp.mapping.select_prev_item(),
		["<Tab>"] = cmp.mapping.confirm({ select = true }),
		["<CR>"] = cmp.mapping({
			i = function(fallback)
				if cmp.visible() and cmp.get_active_entry() then
					cmp.confirm({
						-- cmp.ConfirmBehavior.Replace,
						select = false,
					})
				else
					fallback()
				end
			end,
			s = cmp.mapping.confirm({ select = true }),
			-- c = cmp.mapping.confirm({
			-- 	-- cmp.ConfirmBehavior.Replace,
			-- 	select = true,
			-- }),
		}),
		["<A-Space>"] = cmp.mapping(function(fallback)
			local ls = require("luasnip")
			if ls.expand_or_jumpable() then
				ls.expand()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<A-j>"] = cmp.mapping(function(fallback)
			local ls = require("luasnip")
			if ls.jumpable(1) then
				ls.jump(1)
			else
				fallback()
			end
		end, { "i", "s" }),
		["<A-k>"] = cmp.mapping(function(fallback)
			local ls = require("luasnip")
			if ls.jumpable(-1) then
				ls.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
		["<A-l>"] = cmp.mapping(function(fallback)
			local ls = require("luasnip")
			if ls.choice_active() then
				ls.change_choice(1)
			else
				fallback()
			end
		end, { "i", "s" }),
		["<A-h>"] = cmp.mapping(function(fallback)
			local ls = require("luasnip")
			if ls.choice_active() then
				ls.change_choice(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}
end

local function format_config()
	local lk = require("lspkind")
	return {
		format = lk.cmp_format({
			mode = "symbol_text",
			maxwidth = 50,
			symbol_map = {
				Copilot = "󰚩",
			},
			menu = {
				copilot = "[Copilot]",
				buffer = "[Buf]",
				nvim_lsp = "[LSP]",
				path = "[Path]",
				look = "[Spell]",
			},
		}),
	}
end

return {
	"hrsh7th/nvim-cmp",
	dependencies = {
		"saadparwaiz1/cmp_luasnip",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-cmdline",
		"onsails/lspkind-nvim",
		"hrsh7th/cmp-nvim-lsp-signature-help",
		"zbirenbaum/copilot-cmp",
		"windwp/nvim-autopairs",
	},
	opts = function(_, opts)
		local cmp = require("cmp")
		return {
			window = {
				-- completion = cmp.config.window.bordered(),
				documentation = cmp.config.window.bordered(),
			},
			preselect = cmp.PreselectMode.None,
			snippet = {
				expand = function(args)
					require("luasnip").lsp_expand(args.body)
				end,
			},
			mapping = mapping_config(),
			sources = cmp.config.sources({
				-- {
				-- 	name = "copilot",
				-- 	group_index = 1,
				-- 	priority = 100,
				-- 	entry_filter = function(entry, _)
				-- 		local ctx = require("cmp.config.context")
				-- 		local inComment = ctx.in_treesitter_capture("comment")
				-- 			or ctx.in_syntax_group("Comment")
				-- 		local inString = ctx.in_treesitter_capture("string")
				-- 			or ctx.in_syntax_group("String")
				-- 		local inText = ctx.in_treesitter_capture("text")
				-- 			or ctx.in_syntax_group("Text")
				-- 		return inComment or inString
				-- 	end,
				-- },
				{ name = "nvim_lsp" },
				{ name = "luasnip" },
				{ name = "path" },
				{ name = "nvim_lsp_signature_help" },
			}, { { name = "buffer" } }),
			matching = { disallow_partial_fuzzy_matching = false },
			experimental = { native_menu = false, ghost_text = false },
			formatting = format_config(),
		}
	end,

	config = function(_, opts)
		local cmp = require("cmp")
		cmp.setup(opts)

		cmp.setup.cmdline(":", {
			mapping = cmp.mapping.preset.cmdline(),
			sources = cmp.config.sources(
				{ { name = "cmdline" } },
				{ { name = "path" } }
			),
		})
		cmp.setup.cmdline({ "/", "?" }, {
			mapping = cmp.mapping.preset.cmdline(),
			sources = {
				{ name = "buffer" },
			},
		})

		-- If you want insert `(` after select function or method item
		local cmp_autopairs = require("nvim-autopairs.completion.cmp")
		cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
	end,
	event = { "InsertEnter", "CmdlineEnter" },
}
