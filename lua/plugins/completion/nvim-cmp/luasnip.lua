return {
	"L3MON4D3/LuaSnip",
	dependencies = {
		"rafamadriz/friendly-snippets",
	},

	config = function()
		local ls = require("luasnip")

		-- extending html to angular
		ls.filetype_extend("angular", { "html" })
		local lua_loader = require("luasnip.loaders.from_lua")

		local vs_loader = require("luasnip.loaders.from_vscode")
		local lua_snip_types = require("luasnip.util.types")

		lua_loader.lazy_load({ paths = { "./snippets/" } })

		vs_loader.lazy_load()
		ls.config.set_config({
			history = true,
			updateevents = "TextChanged,TextChangedI",
			delete_check_events = "TextChanged,InsertLeave",
			enable_autosnippets = true,
			ext_opts = {
				[lua_snip_types.choiceNode] = {
					active = { virt_text = { { "●", "GruvboxOrange" } } },
				},
				[lua_snip_types.insertNode] = {
					active = { virt_text = { { "●", "GruvboxBlue" } } },
				},
			},
		})

		vim.keymap.set({ "i", "n" }, "<A-j>", function()
			if ls.jumpable(1) then
				ls.jump(1)
			end
		end, { silent = true, expr = true })
		vim.keymap.set({ "i", "n" }, "<A-k>", function()
			if ls.jumpable(-1) then
				ls.jump(-1)
			end
		end, { silent = true, expr = true })
	end,
	lazy = true,
}
