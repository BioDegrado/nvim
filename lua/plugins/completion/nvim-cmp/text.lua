return {
	"f3fora/cmp-spell",
	dependencies = {
		"octaltree/cmp-look",
		"hrsh7th/nvim-cmp",
	},
	config = function()
		local cmp = require("cmp")
		cmp.setup.filetype("tex", {
			sources = cmp.config.sources({
				{ name = "nvim_lsp" },
				{ name = "luasnip" },
				{ name = "path" },
				{ name = "buffer" },
			}),
		})
	end,
	ft = { "plaintext", "markdown" },
}
