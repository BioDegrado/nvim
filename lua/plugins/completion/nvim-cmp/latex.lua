return {
	"kdheepak/cmp-latex-symbols",
	dependencies = {
		"hrsh7th/nvim-cmp",
		"f3fora/cmp-spell",
		"octaltree/cmp-look",
	},
	config = function()
		local cmp = require("cmp")
		cmp.setup.filetype("tex", {
			sources = cmp.config.sources({
				{ name = "nvim_lsp" },
				{ name = "luasnip" },
				{ name = "path" },
				{ name = "buffer" },
			}),
		})
	end,
	ft = { "tex", "plaintext", "markdown" },
}
