return {
	"mfussenegger/nvim-lint",
	config = function(_)
		local lint = require("lint")
		lint.linters_by_ft = {
			-- typescript = { "eslint_d" },
			-- angular = { "eslint_d" },
		}
		vim.api.nvim_create_autocmd({ "TextChanged" }, {
			pattern = { "*.ts", "*.js", "*.html" },
			callback = function() require("lint").try_lint() end,
		})
	end,
}
