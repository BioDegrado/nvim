return {
	"pmizio/typescript-tools.nvim",
	dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
	opts = {
		settings = {
			separate_diagnostic_server = true,
			publish_diagnostic_on = "insert_leave",
			tsserver_file_preferences = {
				includeCompletionsForModuleExports = true,

				importModuleSpecifierPreference = "absolute",
				importModuleSpecifierEnding = "minimal",

				-- inlay hints
				includeInlayParameterNameHints = "literals",
				includeInlayParameterNameHintsWhenArgumentMatchesName = false,
				includeInlayFunctionParameterTypeHints = true,
				includeInlayVariableTypeHints = true,
				includeInlayVariableTypeHintsWhenTypeMatchesName = false,
				includeInlayPropertyDeclarationTypeHints = true,
				includeInlayFunctionLikeReturnTypeHints = true,
				includeInlayEnumMemberValueHints = true,
			},
			tsserver_format_options = {
				allowIncompleteCompletions = false,
				allowRenameOfImportPath = false,
			},
		},
	},
	lazy = true,
	ft = {
		"typescript",
		"javascript",
		"javascriptreact",
		"typescriptreact",
		"angular",
	},
}
