--- If this is used as the lsp config value, then the configuration
--- of the lsp will be skipped and won't be started.
--- This is generally used for LSPs that need to be managed by other plugins.
local EXIT = -1

local corelsp = require("core.lsp")

---@type table<string, table | nil | integer | fun(table): table>
local SERVER_CONFIGS = {
	dartls = EXIT,
	jdtls = EXIT,
	eslint = EXIT,
	ltex = EXIT,
	html = nil,
	bashls = {
		settings = {
			bashIde = {
				shellcheckPath = vim.fn.stdpath("data")
					.. "/mason/bin/shellcheck",
			},
		},
	},
	gopls = {
		settings = {
			gopls = {
				usePlaceholders = true,
				staticcheck = true,
				-- gofumpt = true,
			},
		},
	},
	clangd = {
		settings = {
			clangd = {
				inlayHints = require("core.lsp").default.get_inlayhints(),
			},
		},
	},
	powershell_es = {
		bundle_path = vim.fn.stdpath("data")
			.. "/mason/packages/powershell-editor-services",
	},
}

return {
	"neovim/nvim-lspconfig",
	dependencies = {
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
		"saghen/blink.cmp",
	},
	config = function()
		require("mason").setup()
		local lspconfig = require("lspconfig")
		local default = corelsp.default.get_config()

		default.capabilities =
			require("blink.cmp").get_lsp_capabilities(default.capabilities)
		require("mason-lspconfig").setup_handlers({
			function(server_name)
				-- NOTE: it cannot assume either 0 nor nil
				local config = SERVER_CONFIGS[server_name] or {}
				if config == EXIT then
					return
				end
				if type(config) == "integer" then
					local msg = string.format(
						"The LSP config for %s is still a number...\n Using the default config.",
						server_name
					)
					vim.notify(msg, vim.log.levels.WARN, {})
					return
				end
				if type(config) == "function" then
					config = config(default)
				else
					local custom = corelsp.get_custom_config(server_name)
					config = corelsp.extends(default, config)
					config = corelsp.extends(config, custom)
				end

				lspconfig[server_name].setup(config)
			end,
		})

		lspconfig.gdscript.setup({})

		vim.diagnostic.config({
			virtual_text = true,
			update_in_insert = false,
			float = true,
		})

		vim.api.nvim_create_autocmd("LspAttach", {
			group = vim.api.nvim_create_augroup("UserLspConfig", {}),
			callback = function() require("config.mappings").lsp() end,
		})
	end,
	event = { "BufReadPre", "BufNewFile" },
}
