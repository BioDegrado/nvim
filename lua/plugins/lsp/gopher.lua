return {
	"olexsmir/gopher.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-treesitter/nvim-treesitter",
	},
	build = function() vim.cmd([[silent! GoInstallDeps]]) end,
	ft = "go",
	keys = {
		{
			"<Leader>gat",
			"<cmd>GoTestAdd<CR>",
			desc = "Go add test function",
		},
		{
			"<Leader>gaj",
			function() require("gopher.api").tags_add("json") end,
			desc = "Add JSON tags to struct",
		},
		{
			"<Leader>gdj",
			function() require("gopher.api").tags_rm("json") end,
			desc = "Remove JSON tags to struct",
		},
		{
			"<Leader>gaf",
			function() require("gopher.api").tags_add("firestore") end,
			desc = "Add Firestore tags to struct",
		},
		{
			"<Leader>gdf",
			function() require("gopher.api").tags_rm("firestore") end,
			desc = "Remove Firestore tags to struct",
		},
		{
			"<Leader>gab",
			function() require("gopher.api").tags_add("bigquery") end,
			desc = "Add Bigquery tags to struct",
		},
		{
			"<Leader>gdb",
			function() require("gopher.api").tags_rm("bigquery") end,
			desc = "Remove Bigquery tags to struct",
		},
		{
			"<Leader>gm",
			function()
				local fmt = string.format
				local package = vim.fn.expand("%:h:t")
				local choice = vim.fn.confirm(
					fmt([[Create Local package "%s"?]], package, 1),
					"&Yes\n&No\n&Cancel"
				)
				if choice == 1 then
					-- Nothing
				elseif choice == 2 then
					package = vim.fn.input("Insert the name of the package")
				else
					vim.notify("Operation cancelled", "info")
					return
				end
				local Job = require("plenary.job")
				Job
					:new({
						command = "go",
						args = { "mod", "init", package },
						cwd = vim.fn.expand("%:p:h"),
						on_exit = function(_, code)
							if code == 0 then
								vim.notify(
									fmt("Module %q initialized!", package),
									"info"
								)
							else
								vim.notify(
									fmt(
										"Cannot initialize module!\nError code: %d",
										code
									),
									"error"
								)
							end
						end,
					}):start()
			end,
			desc = "Go mod init package",
		},
		{
			"<Leader>gw",
			desc = "Go mod init",
		},
	},
}
