local vim = vim

local FORMATTERS = {
	shellharden = {
		filetypes = { "sh" },
	},
	stylua = {
		filetypes = { "lua" },
		args = {
			"--collapse-simple-statement",
			"FunctionOnly",
			"--sort-requires",
			"--column-width",
			"80",
		},
	},
	clang_format = {
		filetypes = { "c", "cpp", "c#" },
		-- filetypes = {"c", "c#", "cpp", "json", "java", "javascript"},
		args = {
			"--style",
			"{IndentWidth: 4}",
		},
	},
	black = {
		filetypes = { "python" },
		-- args = { "--stdin-filename", "$FILENAME", "--quiet", "-" },
	},
	prettierd = {
		filetypes = {
			"angular",
			"css",
			"flow",
			"graphql",
			"html",
			"json",
			"jsx",
			"javascript",
			"less",
			"markdown",
			"scss",
			"typescript",
			"vue",
			"yaml",
		},
	},
	gofumpt = { filetypes = { "go" } },
	goimports = { filetypes = { "go" } },
	golines = { filetypes = { "go" }, args = { "--max-len=120" } },
	xmlformat = { filetypes = { "xml" }, args = { "--indent", "4" } },
}

local function user_commands()
	vim.api.nvim_create_user_command("FormatDisable", function(args)
		if args.bang then
			vim.b.disable_autoformat = true
		else
			vim.g.disable_autoformat = true
		end
	end, {
		desc = "Disable autoformat on save",
		bang = true,
	})
	vim.api.nvim_create_user_command(
		"FormatFile",
		function(args)
			require("conform").format({ bufnr = args.buf, async = true })
		end,
		{
			desc = "Format the buffer",
			bang = true,
		}
	)
	vim.api.nvim_create_user_command("FormatEnable", function(args)
		if args.bang then
			vim.b.disable_autoformat = false
		else
			vim.g.disable_autoformat = false
		end
	end, {
		desc = "Enable autoformat on save",
		bang = true,
	})
end

local function mappings()
	vim.keymap.set(
		"v",
		"<leader>f",
		function() require("conform").format() end,
		{ desc = "Format range" }
	)
end

local function force_table_insert(collection, element)
	collection = collection or {}
	table.insert(collection, element)
	return collection
end

return {
	"stevearc/conform.nvim",
	dependencies = "williamboman/mason.nvim",
	opts = {
		log_level = vim.log.levels.DEBUG,
		["*"] = { "codespell" },
		format_on_save = function(bufnr)
			-- Disable autoformat on certain filetypes
			local ignore_filetypes = { "sql" }
			if vim.tbl_contains(ignore_filetypes, vim.bo[bufnr].filetype) then
				return
			end

			-- Disable with a global or buffer-local variable
			if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
				return
			end

			local bufname = vim.api.nvim_buf_get_name(bufnr)
			-- Disable autoformat for files in the Work project dir
			-- They don't use formatters :(
			if bufname:match("/Projects/Work/src-") then
				return
			end

			-- Disable autoformat for files in a certain path
			if bufname:match("/node_modules/") then
				return
			end

			return { timeout_ms = 10000, lsp_fallback = true }
		end,
	},
	config = function(_, opts)
		local conform = require("conform")
		local util = require("conform.util")
		conform.setup(opts)
		local filetype_setup = conform.formatters_by_ft

		for formatter, formatter_opts in pairs(FORMATTERS) do
			for _, filetype in ipairs(formatter_opts.filetypes) do
				filetype_setup[filetype] =
					force_table_insert(filetype_setup[filetype], formatter)
				if formatter_opts.args ~= nil then
					local formatter_template = require(
						string.format("conform.formatters.%s", formatter)
					)
					util.add_formatter_args(
						formatter_template,
						formatter_opts.args
					)
				end
			end
		end

		user_commands()
		mappings()
	end,
	event = {
		"VeryLazy",
	},
}
