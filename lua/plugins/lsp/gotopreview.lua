return {
	"rmagatti/goto-preview",
	config = true,
	keys = {
		{
			"gpd",
			function()
				require("goto-preview").goto_preview_definition({
					focus_on_open = true,
				})
			end,
			desc = "Preview definition",
		},
		{
			"gpi",
			function()
				require("goto-preview").goto_preview_implementation({
					focus_on_open = true,
				})
			end,
			desc = "Preview implementation",
		},
		{
			"gpt",
			function()
				require("goto-preview").goto_preview_type_definition({
					focus_on_open = true,
				})
			end,
			desc = "Preview implementation",
		},
		{
			"gpr",
			function() require("goto-preview").goto_preview_references() end,
			desc = "Preview references",
		},
		{
			"gpp",
			function() require("goto-preview").close_all_win() end,
			desc = "Close all preview windows",
		},
	},
}
