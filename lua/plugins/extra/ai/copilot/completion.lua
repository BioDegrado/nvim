-- return {
-- 	enabled = true,
-- 	"zbirenbaum/copilot-cmp",
-- 	dependencies = {
-- 		"zbirenbaum/copilot.lua",
-- 		build = ":Copilot auth",
-- 		opts = {
-- 			suggestion = { enabled = false },
-- 			panel = { enabled = false },
-- 			filetypes = {
-- 				markdown = true,
-- 				help = true,
-- 			},
-- 		},
-- 		config = true,
-- 		cmd = "Copilot",
-- 		event = { "InsertEnter" },
-- 	},
-- 	opts = {},
-- 	config = function(_, opts)
-- 		local copilot_cmp = require("copilot_cmp")
-- 		copilot_cmp.setup(opts)
-- 		-- -- attach cmp source whenever copilot attaches
-- 		-- -- fixes lazy-loading issues with the copilot cmp source
-- 		-- require("lazyvim.util").lsp.on_attach(function(client)
-- 		-- 	if client.name == "copilot" then
-- 		-- 		copilot_cmp._on_insert_enter({})
-- 		-- 	end
-- 		-- end)
-- 	end,
-- }

return {
	"github/copilot.vim",
	dependencies = { "catppuccin/nvim" },
	event = "VimEnter",
	init = function()
		vim.g.copilot_no_tab_map = true
		vim.g.copilot_workspace_folders = { vim.fn.getcwd() }
		vim.keymap.set(
			"i",
			"<S-Tab>",
			'copilot#Accept("\\<CR>")',
			{ expr = true, silent = true, replace_keycodes = false }
		)
		vim.keymap.set(
			"i",
			"<M-]>",
			[[copilot#Next()]],
			{ expr = true, silent = true }
		)
	end,
}
