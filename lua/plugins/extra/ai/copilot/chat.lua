return {
	"CopilotC-Nvim/CopilotChat.nvim",
	event = "VeryLazy",
	branch = "main",
	dependencies = {
		{ "github/copilot.vim" }, -- or zbirenbaum/copilot.lua
		{ "nvim-lua/plenary.nvim", branch = "master" }, -- for curl, log and async functions
	},
	opts = {
		prompts = {
			-- default prompts
			prompts = {
				Explain = {
					prompt = "/COPILOT_EXPLAIN Write an explanation for the active selection as paragraphs of text.",
				},
				Review = {
					prompt = "/COPILOT_REVIEW Review the selected code.",
				},
				Fix = {
					prompt = "/COPILOT_GENERATE There is a problem in this code. Rewrite the code to show it with the bug fixed.",
				},
				Optimize = {
					prompt = "/COPILOT_GENERATE Optimize the selected code to improve performance and readability.",
				},
				Docs = {
					prompt = "/COPILOT_GENERATE Please add documentation comment for the selection.",
				},
				Tests = {
					prompt = "/COPILOT_GENERATE Please generate tests for my code.",
				},
				Mocks = {
					prompt = "/COPILOT_GENERATE Please generate mocks for my code.",
				},
				Logs = {
					prompt = "/COPILOT_GENERATE Please put logs inside my function.",
				},
			},
		},
	},
	build = function()
		vim.notify(
			"Please update the remote plugins by running ':UpdateRemotePlugins', then restart Neovim."
		)
	end,
	config = function(_, opts)
		local present, wk = pcall(require, "which-key")
		if not present then
			return
		end

		wk.add({
			{
				"<leader>cc",
				group = "Copilot Chat",
				nowait = false,
				remap = false,
			},
		})
		require("CopilotChat").setup(opts)
	end,
	keys = {
		{
			"<leader>ccc",
			":CopilotChat ",
			desc = "CopilotChat - Prompt",
		},
		{
			"<leader>ccm",
			":CopilotChatMock<cr>",
			desc = "CopilotChat - Mock",
		},
		{
			"<leader>cce",
			":CopilotChatExplain<cr>",
			desc = "CopilotChat - Explain code",
		},
		{
			"<leader>cct",
			"<cmd>CopilotChatTests<cr>",
			desc = "CopilotChat - Generate tests",
		},
		{
			"<leader>ccr",
			"<cmd>CopilotChatReview<cr>",
			desc = "CopilotChat - Review code",
		},
		{
			"<leader>ccd",
			"<cmd>CopilotChatDocs<cr>",
			desc = "CopilotChat - Review code",
		},
		{
			"<leader>ccR",
			"<cmd>CopilotChatRefactor<cr>",
			desc = "CopilotChat - Refactor code",
		},
		{
			"<leader>ccl",
			"<cmd>CopilotChatLogs<cr>",
			desc = "CopilotChat - Generate Logs code",
		},
	},
}
