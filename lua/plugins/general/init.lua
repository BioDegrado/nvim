return {
	{ "tpope/vim-fugitive" },
	{
		"stevearc/oil.nvim",
		config = true,
		keys = {
			{
				"-",
				"<CMD>Oil<CR>",
				desc = "Open parent directory",
			},
		},
	},
	-- {
	-- 	"echasnovski/mini.ai",
	-- 	version = "*",
	-- 	config = true,
	-- 	opts = {
	--
	-- 		-- Table with textobject id as fields, textobject specification as values.
	-- 		-- Also use this to disable builtin textobjects. See |MiniAi.config|.
	-- 		custom_textobjects = nil,
	--
	-- 		-- Module mappings. Use `''` (empty string) to disable one.
	-- 		mappings = {
	-- 			-- Main textobject prefixes
	-- 			around = "a",
	-- 			inside = "i",
	--
	-- 			-- Next/last variants
	-- 			around_next = "an",
	-- 			inside_next = "in",
	-- 			around_last = "al",
	-- 			inside_last = "il",
	--
	-- 			-- Move cursor to corresponding edge of `a` textobject
	-- 			goto_left = "g[",
	-- 			goto_right = "g]",
	-- 		},
	--
	-- 		-- Number of lines within which textobject is searched
	-- 		n_lines = 300,
	--
	-- 		-- How to search for object (first inside current line, then inside
	-- 		-- neighborhood). One of 'cover', 'cover_or_next', 'cover_or_prev',
	-- 		-- 'cover_or_nearest', 'next', 'previous', 'nearest'.
	-- 		search_method = "cover_or_next",
	--
	-- 		-- Whether to disable showing non-error feedback
	-- 		silent = false,
	-- 	},
	-- },
	{
		"iamcco/markdown-preview.nvim",
		build = function() vim.fn["mkdp#util#install"]() end,
		ft = "markdown",
		cmd = {
			"MarkdownPreviewToggle",
			"MarkdownPreview",
			"MarkdownPreviewStop",
		},
		init = function()
			local is_windows = require("utils.os").is_windows()
			local windows_browser =
				"C:\\Program Files\\Mozilla Firefox\\private_browsing.exe"
			vim.g.mkdp_filetypes = { "markdown" }
			vim.g.mkdp_browser = is_windows and windows_browser or ""
		end,
	},
	{
		"phaazon/hop.nvim",
		config = true,
		keys = {
			{
				"gl",
				"<cmd>HopChar1<CR>",
				desc = "Jump to a visible char in the buffer.",
			},
		},
	},
	{ "windwp/nvim-autopairs", config = true, event = { "InsertEnter" } },
	{
		"kylechui/nvim-surround",
		config = true,
		opts = {
			keymaps = {
				insert = "<C-g>s",
				insert_line = "<C-g>S",
				normal = "s",
				normal_cur = "ss",
				normal_line = "S",
				normal_cur_line = "SS",
				visual = "S",
				visual_line = "gS",
				delete = "ds",
				change = "cs",
				change_line = "cS",
			},
		},
		version = "*",
	},
	{
		"ThePrimeagen/git-worktree.nvim",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
		},
		config = function()
			require("git-worktree").setup()
			require("telescope").load_extension("git_worktree")
		end,
		keys = {
			{
				"<leader>fw",
				function()
					require("telescope").extensions.git_worktree.create_git_worktree()
				end,
			},
		},
	},
	{
		"mistricky/codesnap.nvim",
		build = "make",
		keys = {
			{
				"<leader>s",
				function() vim.cmd([['<,'>CodeSnap]]) end,
				mode = "x",
				desc = "Save selected code snapshot into clipboard",
			},
			{
				"<leader>S",
				function() vim.cmd([['<,'>CodeSnapSave]]) end,
				mode = "x",
				desc = "Save selected code snapshot in ~/Pictures",
			},
		},
		opts = {
			save_path = "~/Pictures",
			title = ">.>",
			watermark = "",
			has_breadcrumbs = true,
			has_line_number = true,
			bg_theme = "bamboo",
			bg_x_padding = 32,
			bg_y_padding = 16,
		},
	},
}
