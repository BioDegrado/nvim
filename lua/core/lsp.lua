local M = {
	--- Contains functions that return the default capabilities for the client
	default = {},
}

local srequire = require("core").safe_require

--- Returns the default capabilities for completion
---@return table
local function cmp_default_capabilities()
	local ok, cmp_lsp = srequire("cmp_nvim_lsp")
	if not ok then
		return {}
	end
	---@type table
	return cmp_lsp.default_capabilities()
end

--- Returns the default capabilities of the client
--- If it this needs to be used for a lspconfig, then it's best
--- to use the `get_default_config` function instead.
---@return table
M.default.get_capabilities = function()
	local capabilities = vim.lsp.protocol.make_client_capabilities() or {}
	capabilities.textDocument.foldingRange = {
		dynamicRegistration = false,
		lineFoldingOnly = true,
	}
	local completion = cmp_default_capabilities()
	capabilities = vim.tbl_deep_extend("force", capabilities, completion) or {}

	return capabilities
end

--- Returns the default configuration for the LSP client
--- @return table
M.default.get_config = function()
	local ok, lsp = srequire("lspconfig")
	if not ok then
		return {}
	end
	local config = lsp.util.default_config
	local capabilities = M.default.get_capabilities()
	config.capabilities =
		vim.tbl_deep_extend("force", config.capabilities, capabilities)
	return config
end

--- Get a custom configuration for a LSP server by processing it from a lua script
--- @param server_name string The name of the server
--- @return table | nil
M.get_custom_config = function(server_name)
	local dynamic_settings = "config.lsp." .. server_name
	local ok, config = srequire(dynamic_settings)
	return ok and config or nil
end

M.default.get_inlayhints = function()
	return {
		includeInlayEnumMemberValueHints = true,
		includeInlayFunctionLikeReturnTypeHints = true,
		includeInlayFunctionParameterTypeHints = true,
		includeInlayParameterNameHints = "all", -- 'none' | 'literals' | 'all';
		includeInlayParameterNameHintsWhenArgumentMatchesName = true,
		includeInlayPropertyDeclarationTypeHints = true,
		includeInlayVariableTypeHints = true,
	}
end

--- Merge a default configuration with an extension
--- @param config table The default configuration
--- @param extension table | nil The extension to apply to the configuration
--- @return table The merged configuration
M.extends = function(config, extension)
	config = config or {}
	if extension == nil then
		return config
	end
	return vim.tbl_deep_extend("force", config, extension) or {}
end

M.disable_code_actions = function(config)
	if config.capabilities == nil or config.capabilities.textDocument == nil then
		return
	end
	config.capabilities.textDocument.codeAction = {}
	return config
end

return M
