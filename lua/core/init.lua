--- @module 'core'
--- Module with some additional functions
--- that can be considered fundamental
--- for the correct functioning of this configuration
local M = {}

---@param module string the module to convert
---@return boolean
local function config_exists(module)
	local lua_path = vim.fn.stdpath("config") .. "/lua/"
	local mod_path = lua_path .. string.gsub(module, "[.]", "/") .. ".lua"
	return require("utils.fs").file_exists(mod_path)
end

---@class Error
---@field code integer
---@field message string

--- This function allows to require a module without crashing
--- the program. If error occurs then returns error status
--- @param module string module to require
--- @return boolean ok if the 'require' has executed correctly
--- @return any|Error res error message of the 'require' failure. If ok returns the result
M.safe_require = function(module)
	local ok, res = pcall(require, module)
	if not ok then
		---@type Error
		res = {
			code = 1,
			message = string.format("Cannot load module[%s]: %s", module, res),
		}
	end
	return ok, res
end

M.load_os_configs = function()
	local mod_pre = "config.os."
	local os_modules = {
		WINDOWS = mod_pre .. "windows",
		LINUX = mod_pre .. "linux",
		MAC = mod_pre .. "mac",
	}

	local os_name = require("utils.os").get_os()
	local found = false
	for name, os_config in pairs(os_modules) do
		if config_exists(os_config) then
			if os_name == name then
				found, _ = M.print_on_error(M.safe_require, { os_config })
			end

			if found then
				-- vim.notify("Loaded os config: " .. name)
				return
			end
		end
	end
	-- vim.notify("no os config found for " .. os_name)
end

--- Given a function which returns an ok and a response,
--- 'print_on_error' tries to run it and prints the error message if
--- an error occurs ad returns all of its values.
--- @param fun function the callable function which returns an (ok and resp)
--- @param args table | any
--- @param prefix string?
--- @return boolean ok if 'fun' has executed correctly
--- @return string|any res error message 'fun' failed, returns the result otherwise
M.print_on_error = function(fun, args, prefix)
	args = type(args) == "table" and args or { args }
	local ok, res = fun(unpack(args))
	if not ok then
		prefix = prefix and prefix or ""
		vim.print(prefix .. res.message)
	end
	return ok, res
end

return M
