local M = {}
local core = require("core")

--- @type table<string, (fun(any): nil)[]>
local callbacks = {}

--- Run all callbacks for a given name
--- @param name string
--- @return Error|nil
M.run_config_callbacks = function(name, mod)
	if callbacks[name] == nil then
		return { code = 1, message = "No callbacks for " .. name }
	end

	for _, callback in ipairs(callbacks[name]) do
		callback(mod)
	end
	return nil
end

M.when_ready = function(name, callback)
	callbacks[name] = callbacks[name] or {}

	if callback then
		callbacks[name][#callbacks[name] + 1] = callback
	end

	local ok, mod = core.safe_require(name)
	if not ok then
		return
	end

	local err = M.run_config_callbacks(name, mod)
	if err then
		print("Error in running the callbacks for config: " .. name)
		vim.print("Error: ", err.message)
	end
	callbacks[name] = {}
	return nil
end

return M
