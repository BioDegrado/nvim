local filetype = vim.api.nvim_create_augroup("web", { clear = true })
vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	pattern = { "*.html" },
	callback = function()
		-- recomputing each file because i can open html files outside the project
		local is_angular_project = vim.fs.find(
			{ "angular.json", "ionic.config.json" },
			{ upward = true, stop = vim.env.HOME }
		)[1]
		if is_angular_project then
			vim.opt_local.filetype = "angular"
			vim.opt_local.commentstring = "<!-- %s -->"
		end
	end,
	group = filetype,
})

vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
	pattern = { "*.conf" },
	callback = function()
		-- recomputing each file because i can open html files outside the project
		vim.fs.dirname("")
		local is_hypr_config =
			vim.fs.find({ "hypr" }, { upward = true, stop = vim.env.HOME })[1]
		if is_hypr_config then
			vim.opt_local.filetype = "hyprlang"
		end
	end,
	group = filetype,
})
