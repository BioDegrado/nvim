local autogroup = vim.api.nvim_create_augroup("rasi filetype", { clear = true })
vim.api.nvim_create_autocmd(
	{ "Bufread", "BufNewFile" },
	{ pattern = "*.rasi", command = "setl ft=rasi", group = autogroup }
)
