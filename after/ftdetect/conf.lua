local autogroup =
	vim.api.nvim_create_augroup("config filetype", { clear = true })
vim.api.nvim_create_autocmd(
	{ "Bufread", "BufNewFile" },
	{ pattern = "*.conf", command = "setl ft=conf", group = autogroup }
)
