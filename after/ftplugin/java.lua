local jdtls = require("jdtls")

local MASON = vim.fn.stdpath("data") .. "/mason/packages/"
local JDTLS_PATH = MASON .. "jdtls/"
local DEBUG_PATH = MASON .. "java-debug-adapter/extension/server/"
local TEST_PATH = MASON .. "java-test/extension/server/"
local LAUNCHER_PATH =
	vim.fn.glob(JDTLS_PATH .. "plugins/org.eclipse.equinox.launcher_*.jar")
CONFIG = {
	WINDOWS = JDTLS_PATH .. "config_win",
	LINUX = JDTLS_PATH .. "config_linux",
}

local found_markers =
	vim.fs.find({ "gradlew", ".git", "mvnw", "pom.xml" }, { upward = true })[1]
local root_dir = vim.fs.dirname(found_markers)
local workspace_path = vim.fn.stdpath("cache") .. "/nvim-djtls/"

local extendedClientCapabilities = jdtls.extendedClientCapabilities
extendedClientCapabilities.onCompletionItemSelectedCommand =
	"editor.action.triggerParameterHints"
extendedClientCapabilities.resolveAdditionalTextEditsSupport = true

local os = require("utils.os").get_os()
local config = {}
config.root_dir = root_dir
config.settings = { java = require("config.lsp.jdtls") }
config.cmd = {
	"java",
	"-Declipse.application=org.eclipse.jdt.ls.core.id1",
	"-Dosgi.bundles.defaultStartLevel=4",
	"-Declipse.product=org.eclipse.jdt.ls.core.product",
	"-Dlog.protocol=true",
	"-Dlog.level=ALL",
	"-javaagent:" .. JDTLS_PATH .. "lombok.jar",
	"-Xmx1g",
	"--add-modules=ALL-SYSTEM",
	"--add-opens",
	"java.base/java.util=ALL-UNNAMED",
	"--add-opens",
	"java.base/java.lang=ALL-UNNAMED",
	"-jar",
	LAUNCHER_PATH,
	"-configuration",
	CONFIG[os],
	"-data",
	workspace_path .. vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t"),
}

local function files(glob) return vim.split(vim.fn.glob(glob, true), "\n") end

local debug_jars = files(DEBUG_PATH .. "com.microsoft.java.debug.plugin-*.jar")
local test_jars = files(TEST_PATH .. "*.jar")

-- with dependencies plugin shouldn't be loaded for jdtls
for i, test in ipairs(test_jars) do
	if test:find([[dependencies]]) then
		table.remove(test_jars, i)
	end
end

config.init_options = {
	bundles = vim.fn.extend(debug_jars, test_jars),
	extendedClientCapabilities = extendedClientCapabilities,
}
config.on_attach = function()
	local jdap = require("jdtls.dap")
	jdap.setup_dap({ hotcodereplace = "auto" })
	jdap.setup_dap_main_class_configs()
	-- jdtls.setup.add_commands()

	vim.keymap.set(
		"n",
		"<Leader>ddt",
		function() jdap.test_nearest_method() end,
		{ desc = "Debug the nearest test function." }
	)
	vim.keymap.set(
		"n",
		"<Leader>ddT",
		function() jdap.test_class() end,
		{ desc = "Debug the current class." }
	)
end

jdtls.start_or_attach(config)
